'use strict'
// Iterate over the colors. Find the one at max. Turn it off,
// then turn the next one on. Repeat until ctrl-c.
if (!process.getuid && process.getuid() === 0) {
  console.error('The program must be run as root.')
}
const ctl = require('../daemon/modules/control')

console.log('Control Init Complete')
console.log('R - G - B - WW - CW @ 1 sec ea.')
console.log('Press Ctl-C to Exit')

let colors = ['red', 'green', 'blue', 'warm', 'cool']
ctl.set({red: 255})
let nextFlag = false

let int = setInterval(() => {
  let obj = {}
  colors.forEach(color => {
    if (nextFlag) {
      nextFlag = false
      obj[color] = 255
      ctl.set(obj[color] = 255)
    } else if (!nextFlag && (ctl.led[color].level > 0)) {
      nextFlag = true
      let obj = {}
      ctl.set(obj[color] = 0)
    }
  })
}, 1000)

process.on('SIGINT', () => {
  console.log('\nExiting...')
  clearInterval(int)
  ctl.set({
    red: 0,
    green: 0,
    blue: 0,
    warm: 0,
    cool: 0
  })
  process.exit()
})
