### To-Do
- Add the transition building system to the front end
- Test transition system clean up in the daemon
- Make a deploy script? Is that necessary? Or just some detailed instructions? hmmm...
- Add the alarm/timer system
- Add in a slight delay for delivering changes from the sliders, because it spams the web server.
- Add in a checkmark for successful sets beside LightPi (or an overlay?)
- Change toggle color so it's clear whether you're turning things off or on.

### Bugs

### Oct 16 2018
- Added in the transition and alarm endpoints. Also wrote some code to back transition.
- No code yet for alarms, this will be a whole thing. Tempted to set up another child process.
- Split apart the front end JS for easier coding. ESLint doesn't like it, though

### Oct 12 2018
- Cut out all of the jquery-ui crap sometime a few commits ago.
- Front end can now talk to back end with the sliders.
- Rewrote the transition system to be able to chain. Uses reduce (yay!)
- Added in the framework for the alarms
- Cleaned up mobile formatting (still more to go)

### Sept 28 2018
- Delivering some legit html now
- added the fake daemon and 'webonly' flag (`sudo node index.js webonly`) so i could code the web stuff off the pi
- manifest and favicons should be ok

### Sept 24/25 2018
- Have something working now! Yay!
- Can send files en masse out of `/web-server/public` (and cache them to boot).
- Daemon seems to work properly, except for transition bug above.
- Console debug is wonderfully descriptive and colourful, and is disabled when it's off except for the 'started' message.
- Cleaned up all the nonsense with adding in promises where they weren't needed.

### Oct 3 2017
- Adapting from barebones. Still have private copy of other more complicated, never-properly-working-version-1.
- Thought I'd throw it up on Github.
- Yeeeepp.
