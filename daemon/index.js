'use strict'
/*
All of the parsing and control happens at this level. Here we prioritize:
Off, On, Toggle, Freeze, Transition. These are all wrappers around
daemon.control.set, which isn't directly accessible.
All the sub-modules have 'this' set to the daemon itself, so they can
update with this.state and control with this.control.set(...)
*/
const cfg = require('../config/daemon-config')
if (cfg.debug) { const colors = require('colors') }
const operations = [
  'off',
  'freeze',
  'transition',
  'on',
  'toggle',
  'set',
  'status',
  'utility'
]

if (cfg.debug) process.stdout.write('Starting Daemon... '.cyan)
let daemon = {
  control: require('./modules/control'),
  state: {
    transition: 0,
    frozen: false,
    status: 'ok',
    operation: 'none',
  },
  timer: null,
  tArray: [],
  stepLength: 0
}

operations.forEach(op => {
  daemon[op] = require(`./modules/${op}.js`)
})

process.on('message', function (message) {
  daemon.state.status = 'rejected'
  daemon.state.operation = 'none'
  operations.some(op => {
    if (message[op]) {
      if (daemon.utility.isEmptyObject(message[op])) {
        return true
      }
      daemon.state.operation = op
      let log = daemon[op].call(daemon, message[op])
      if (cfg.debug) console.log('Daemon-module log says:'.cyan, log)
      return true
    }
  })
  process.send({colors: daemon.control.ledState(), ...daemon.state})
})

process.on('disconnect', function () {
  if (cfg.debug) console.log('Daemon has disconnected.'.cyan)
  process.exit(0)
})

if (cfg.debug) process.stdout.write('Daemon started.\n'.cyan)
