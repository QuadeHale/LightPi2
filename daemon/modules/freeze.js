'use strict'

let freeze = function (value) {
  let state = this.state
  if (value) {
    if (this.timer) {
      clearTimeout(this.timer)
      state.frozen = true
      state.status = 'ok'
    } else {
      state.status = 'rejected'
      return '"frozen" error: frozen called when no transition'
    }
  } else {
    state.frozen = false
    this.timer = setTimeout(this.transition.step(), this.stepLength)
  }
}

module.exports = freeze
