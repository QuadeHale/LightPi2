'use strict'
let set = function (obj) {
  let state = this.state
  if ((state.transition > 0) && (!state.freeze)) {
    state.state = 'rejected'
    return '"set" error: active transition'
  }

  if (this.control.set(obj)) {
    state.status = 'ok'
    return '"set" ok'
  } else {
    state.status = 'rejected'
    return '"set" error: control returned falsey'
  }
}

module.exports = set
