'use strict'

let off = function (obj) {
  let state = this.state
  let colors = this.control.colors
  let colorObj = {}

  if (Array.isArray(obj)) {
    obj.forEach((color) => {
      if (colors.includes(color)) {
        colorObj[color] = 0
      }
    })
  } else if (obj) {
    colors.forEach(color => {
      colorObj[color] = 0
    })
  }

  if (!this.utility.isEmptyObject(colorObj)) {
    if (this.timer) {
      clearTimeout(this.timer)
      delete this.timer
      state.freeze = false
    }
  } else {
    state.status = 'rejected'
    return '"off" error: message resulted in no change'
  }

  if (this.control.set(colorObj)) {
    state.status = 'ok'
    return '"off" ok'
  } else {
    state.status = 'rejected'
    return '"off" error: control returned falsey'
  }
}

module.exports = off
