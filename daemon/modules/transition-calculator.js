'use strict'
const conf = require('../../config/daemon-config')

let transitionCalculator = function (acc, to) {
  // Preloading start
  if (acc.length < 1) return to
  let steps = 0
  let transitionArray = []
  let from = acc[acc.length-1]
  // Calculate which color has the furthest to travel
  this.control.colors.forEach(color => {
    let diff = Math.abs(from[color] - to[color])
    if (diff > steps) { steps = diff }
  })
  console.log('Transition Steps:', steps)

  // Calculate
  for (let i = 0; i <= steps; i++) {
    let colorObj = {}
    conf.colors.forEach(color => {
      // For each color we're transitioning, calculate the difference.
      let calcColor = Math.floor( (to[color]-from[color]) / steps * i) + from[color]
      colorObj[color] = calcColor
    })
    console.log('Color Object:', colorObj)
    transitionArray.push(colorObj)
  }
  return transitionArray
}

module.exports = transitionCalculator
