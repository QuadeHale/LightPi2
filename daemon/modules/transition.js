'use strict'
const calc = require('./transition-calculator')
const conf = require('../../config/daemon-config')

let transition = function (tArr) {
  let state = this.status
  state.status = 'rejected'
  tArr = transition.sanitize(tArr)
  if (typeof(tArr) === 'string') return tArr

  state.status = 'ok'
  // Generate transition
  this.tArray = tArr.reduce(calc, [])
  // Calculate ms between calls
  this.stepLength = tArr.duration / this.tArray.length
  console.log(this.tArray[this.tArray.length/2])
  // Begin
  this.control.set(this.tArray.shift())
  status.frozen = false
  this.timer = setTimeout(() => { transition.step() }, this.stepLength)
}

transition.step = function () {
  this.array = this.control.set(this.array.shift())
  this.status.transition -= this.stepLength
  if (!this.tArray.length) {
    this.status.transition = 0
    this.timer = null
    this.frozen = false
  }
  this.timer = setTimeout(() => { transition.step() })
}

transition.sanitize = function (tArr) {
  let nowColor = this.control.ledState()
  if (!Array.isArray(tArr)) return '"transition" error: transition is not an array'
  if (tArr.length < 2) return '"transition" error: transition must contain at least two color objects'
  for (let i = 0; i < tArr.length; i++) {
    let tObj = tArr[i]
    if (i === 0) {
      // For the from object
      if (!this.utility.isColorObject(tObj)) return '"transition" error: first item isn\'t a color object'
      // Backfill colors from current
      conf.colors.forEach(color => {
        if (!tObj[color]) tObj[color] = nowColor[color]
      })
    } else {
      // For each to object
      if (!tObj.duration) return '"transition" error: index '+i+' contains no duration'
      if (isNaN(tObj.duration)) return '"transition" error: index '+i+'\'s duration is not a number'
      tObj.duration = Math.floor(tObj.duration)
      if (tObj.duration > conf.maxDuration) tObj.duration = conf.maxDuration
      if (tObj.duration < conf.minDuration) tObj.duration = conf.minDuration
      // Backfill colors from previous
      conf.colors.forEach(color => {
        if (!tObj[color]) tObj[color] = tArr[i-1][color]
      })
    }
    let cleanObj = {}
    Object.keys(tObj).forEach(key => {
      if ([...conf.colors, 'duration'].includes(key)) cleanObj[key] = tObj[key]
    })
    tObj = cleanObj
  }
  return tArr
}

module.exports = transition
