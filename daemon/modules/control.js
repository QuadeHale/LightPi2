'use strict'

const gpio = require('pigpio').Gpio
const cfg = require('../../config/daemon-config')

let control = { led: {} }

control._init = function () {
  cfg.colors.forEach(color => {
    control.led[color] = {}
    control.led[color].gpio = new gpio(cfg[color].gpio, {mode: gpio.OUTPUT})
    control.led[color].gpio.pwmWrite(0)
    control.led[color].level = 0
  })
  control.colors = cfg.colors
  return true
}

control.set = (obj) => {
  let worked = false
  Object.keys(obj).forEach(color => {
    if (cfg.colors.includes(color)) {
      worked = true
      let level = obj[color]
      if (cfg.debug) { console.log('Control: Setting', color, 'to', level, '.') }
      control.led[color].gpio.pwmWrite(level)
      control.led[color].level = level
    }
  })
  if (worked) { return true }
  else { return true }
}

control.ledState = () => {
  let obj = {}
  Object.keys(control.led).forEach(color => {
    obj[color] = control.led[color].level
  })
  return obj
}

control._init()

module.exports = control
