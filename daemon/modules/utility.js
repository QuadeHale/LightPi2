'use strict'
const cfg = require('../../config/daemon-config')
let utility = {}

utility.isEmptyObject = function (obj) {
  if (typeof(obj) === 'object') {
    if (Object.keys(obj).length < 1) {
      return true
    }
  }
  return false
}

utility.isColorObject = function (obj) {
  let value = false
  if (typeof(obj) === 'object') {
    if (!utility.isEmptyObject(obj)) {
      Object.keys(obj).forEach(color => {
        if (cfg.colors.includes(color)) {
          value = true
        }
        if (isNaN(obj[color])) return false
      })
    }
  }
  return value
}

module.exports = utility
