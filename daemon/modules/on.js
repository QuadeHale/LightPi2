'use strict'

let on = function (obj) {
  let state = this.state
  let colors = this.control.colors
  let colorObj = {}

  if ((state.transition > 0) && (!state.freeze)) {
    state.status = 'rejected'
    return '"on" error: active transition'
  }

  if (Array.isArray(obj)) {
    obj.forEach((color) => {
      if (colors.includes(color)) {
        colorObj[color] = 255
      }
    })
  } else if (typeof(obj) === 'string') {
    if (colors.includes(obj)) {
      colorObj[obj] = 255
    }
  } else if (obj === 'all') {
    colors.forEach(color => {
      colorObj[color] = 255
    })
  }

  if (this.control.set(colorObj)) {
    state.status = 'ok'
    return '"on" ok'
  } else {
    state.status = 'rejected'
    return '"on" error: control returned falsey'
  }
}

module.exports = on
