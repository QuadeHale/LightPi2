'use strict'
let toggle = function (obj) {
  let state = this.state
  let colors = this.control.colors
  let colorObj = {}

  if ((state.transition > 0) && (!state.freeze)) {
    state.status = 'rejected'
    return '"toggle" error: active transition'
  }

  let flip = color => {
    if (this.control.led[color].level > 0) {
      colorObj[color] = 0
    } else {
      colorObj[color] = 255
    }
  }

  if (Array.isArray(obj)) {
    obj.forEach((color) => {
      if (colors.includes(color)) {
        flip(color)
      }
    })
  } else if (typeof(obj) === 'string') {
    if (colors.includes(obj)) {
      flip(obj)
    }
  } else if (obj === 'all') {
    colors.forEach(color => {
      flip(color)
    })
  }
  if (this.control.set(colorObj)) {
    state.status = 'ok'
    return '"toggle" ok'
  } else {
    state.status = 'rejected'
    return '"toggle" error: control returned falsey'
  }
}

module.exports = toggle
