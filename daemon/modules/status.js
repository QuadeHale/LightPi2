'use strict'

let status = function (obj) {
  let state = this.state
  if (obj) {
    state.status = 'ok'
    return '"status" ok'
  } else {
    state.status = 'rejected'
    return '"status" error: value not truthy'
  }
}

module.exports = status
