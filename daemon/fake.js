process.on('message', function () {
  process.send(JSON.parse('{"red":0,"green":0,"blue":0,"warm":0,"cool":0,"transition":0,"frozen":false,"status":"ok","operation":"fake"}'))
})

process.on('disconnect', function () {
  console.log('FakeDaemon has disconnected.'.cyan)
  process.exit(0)
})
