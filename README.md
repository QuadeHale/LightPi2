# LightPi2

LightPi2 is a multi-process NodeJS program designed to run on the Raspberry Pi. Because the RPi is slow, I wanted to write all components by hand and optimize for speed with minimal packages - except for MomentJS, which is basically perfect in every way.

The daemon process receives commands from the web server process on how to control the lights, and returning a status when queried. It runs at 'real time'. It is forked by the web server. The daemon, in turn, forks a controller process. This consumes more RAM, but makes everything feel snappy.

The web server process hands over a static page on any `GET` request to anything other than `/lights`.

`/lights` is the API endpoint for the front-end. It listens for RESTful requests from  a browser, and passes them on to the daemon. It is also responsible for alarm management.

### Installation
Clone the repo, then simply type `./setup` which will install the required software - NodeJS, pigpio and NPM modules. It also registers a systemctl service (called `LightPi2`). It will then reboot the pi.

### Configuration
In the config folder, you'll find two files: `daemon-config.js` and `server-config.js`.

#### daemon-config
You'll want to configure each color, and what pin each color is on. Note that we are using the board pin-outs here. The config is pretty self-explanatory; each color needs an entry in the colors array, and then a matching entry as a sub-object with the GPIO. Theoretically, you can put whatever you want in there.

To test your config, you can run `oneoff/testlights.js` which will simply turn on, and then off, the colors as specified in the array on the half-second until you ctrl-c out.

#### server-config
Here you specify port, which host you are listening on, and `debug` just prints out what the front-end is doing at all times so you can keep tabs on things.

## Daemon Commands
#### Basics
The daemon responds to process messages (`process.send`). Any higher order functionality is handled in the web service.
To use it, fork the process, and start passing it objects that look something like this:
```Javascript
{
  'set': {
    'red': 140;
    'blue': 88
  }
}
```
The object is called a *color object* and is simply composed of keys with registered color names and values between 0 and 255. Values above or below these will be truncated to the minimum or maximum value (0 and 255). If you send values that aren't numbers, the color object is invalid and will be rejected.
If you fail to send along a color object, your command will be immediately rejected.
The daemon sends back an object, that looks like this:
```Javascript
{
  'state': "ok",
  'operation': "on",
  'colors': {
    'red': 125,
    'green': 100,
    'blue': 4,
    'white': 0,
  }
  'transition': 0,
  'frozen': false  
}
```
If there is not a transition ongoing, transition will be zero. `transition`'s value is the remaining seconds. Note that it's an estimation; in the case of super-dense transitions [(see below)](#transition) it will be way off, especially on older hardware.

The list of available commands below is in order of precendence; commands higher in the list will prevent ones lower in the list from occurring (or even being processed).
Anything other than these keys are simply ignored. These keys have precedence, with earlier in this list being higher:
- off
- freeze
- transition
- on
- toggle
- set
- status
If the result is not okay, you'll get `rejected` as the state value instead. The `operation` key is one of the keys above. In the case of rejection, `operation` may be `none` if we couldn't determine which function you were after.
### Daemon Specifics
#### Off
`{ off: [<array of colors>] or <truthy value>}` - this shuts off the named colors or in the case of a truthy non-array value, all of the lights. This also cancels any ongoing transition and resets the timer to zero. `'all'`, which you can use in `on` and `toggle` also works here, as it's a truthy value. *This function will not cancel a transition if it wouldn't actually turn anything else off.*
#### Freeze
`{ freeze: true }` - freezes a transition in place. You'll get back the current color object and the remaining transition time. You will now have a frozen transition.
`{ freeze: false }` - resumes the transition.
Note that if you include the `freeze` key and the value isn't `true` or `false` you'll get a `rejected` back. You'll also get a `rejected` back if there isn't a transition.
#### [Transition](#transition)
`{ transition: {} }` - a transition object is always an array:
- The first element (0) is always considered the starting point. When the transition starts, you'll hop immediately to it. A `duration` key is ignored here.
- Each following color object must also contain a `duration` key. This indicates how long it will take to get to this result, *in seconds*. Any values less than 1 and greater than 300 are brought within these limits.
Each transition is divided up in to 'enough' steps so that each change affects light output by one point. The step count is then divided across duration, and an attempt to transition linearly occurs.
Note that on older Pis, trying to jam too many changes in too short of a time will just cause the transition to take longer and there may be some nasty side effects. I recommend splitting your transitions up, using long durations and waiting for transitions to complete before sending another. Allocating 200ms *per point of change* should be sufficient. An example is below.

If you send a transition *while there's currently a transition*, the current transition will be aborted and the new one will start, abruptly jumping from where you were to where you want to be. *Also note that any colors not included are presumed to start and finish at their current values.* If you add a new color in that wasn't in previous ones, it's okay - we'll backfill the color for previous steps.

```Javascript
{
  transition: [
    {
      red: 200,
      green: 10
    },
    {
      red: 50,
      green: 200,
      duration: 20
    },
    {
      red: 0,
      green: 160,
      duration: 40
    }    
  ]
}
```
In this example, we start with the first object, and over the next 20 seconds, transition to the next object, and then on to the next object for 40 seconds, etc...
There is no limit to the extent that you can chain the transitions.
#### On
`{ "on": [<array of color names>]}` or special case `'all'` or a single color, like `{ "on": "green" }`. This will be rejected if there's an ongoing transition. Unsent colors will remain as they are.
#### Toggle
`{ "toggle": <color> }` - if a light is above zero, turn it off. Otherwise, turn it on to 1000. You can also pass it an array of colors like so: `{ "toggle": [ "red", "blue" ]}`, and it will treat each color individually. Invalid keys are ignored. `'all'` also works.
Toggle is a utility for light testing easily. Normal functionality comes from "set".
#### Set
`{ "set": {<color object>} }` - set takes a color object and sets things to those colors. It's that simple! If a color does not exist, it is not set.
#### Status
`{ "status": <truthy> }` - anything not `false`, `undefined` or `null` will get you back the most current status.

### Web Service API
The API only *takes* JSON and only *returns* JSON, and uses the `/lights` endpoint.
You should always `POST` to it. `GET` requests will always return the current status, which also happens to be what you get back from a POST as well.
The JSON is largely the same, adopting the same keys as the daemon. You can `POST` `{ "status": true }` and you'll get back the same thing you would with the `GET` request.

As a rule, you'll get back `202 - Accepted` for all POSTs, `200 - OK` for all GETs. When you see a `{"state": "rejected"}`, you'll get `400 - Bad Request`.
If there's a problem with the daemon, we'll kick back a `500 - Server Error` if we can *burned to the ground*.
