'use strict'

let daemonConfig = {}

daemonConfig.colors = [
  'red',
  'green',
  'blue',
  'warm',
  'cool'
]
daemonConfig.red = { gpio: 23 }
daemonConfig.green = { gpio: 24 }
daemonConfig.blue = { gpio: 25 }
daemonConfig.warm = { gpio: 27 }
daemonConfig.cool = { gpio: 22 }

daemonConfig.debug = true

daemonConfig.maxDuration = 300
daemonConfig.minDuration = 5
daemonConfig.maxValue = 255
daemonConfig.minValue = 0

module.exports = daemonConfig
