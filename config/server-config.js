'use strict'

let serverConfig = {}
serverConfig.port = 80
serverConfig.host = '0.0.0.0'
serverConfig.debug = true
serverConfig.caching = false
serverConfig.transitionFile = 'transitions.list.json'
serverConfig.alarmsFile = 'alarms.list.json'


module.exports = serverConfig
