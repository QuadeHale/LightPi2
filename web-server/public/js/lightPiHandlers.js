$(function () {
  console.log('LightPi Loaded.')
  // TODO: Take this out before production, trigger is bad form
  $('.colorslider').trigger('input')
  $('#new-alarm-time').timepicker({
    'scrollDefault': 'now'
  })
})

$('#power-off').on('click', function () {
  lightctl({off: true})
})

$('.colorslider').on('blur', function () {
  let color = $(this).parent().data('color')
  let val = $(this).val()
  lightctl({set: { [color]: val } })
})

// $('.colorslider').on('input change', updateUI)

$('.colortoggle').on('click', function () {
  let color = $(this).parent().data('color')
  lightctl({toggle: color})
})
