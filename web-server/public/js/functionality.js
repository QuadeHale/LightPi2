function statefix (r) {

}

function updateUI () {
  // Update all sliders to their real values as passed by a color object.
  let color = $(this).parent().data('color')
  let val = $(this).val()
  let valOutput = $(this).siblings('.colorvalue')
  valOutput.text((val / 2.55).toPrecision(3) + '%')
}

function lightctl (cmd, method) {
  return fetch('/lights', {
    method: method || 'POST',
    cache: 'no-cache',
    mode: 'same-origin',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: JSON.stringify(cmd),
  })
    .then( x => { return x.json() })
    .then(statefix)
}
