'use strict'

const fs = require('fs')
const cfg = require('../config/server-config')
const colors = require('colors')

let webServer = {
  files: {},
  transitions: require('./transitions'),
  alarms: require('./alarms'),
  daemon: null
}

webServer.headers = function (req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Request-Method', '*')
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET')
  res.setHeader('Access-Control-Allow-Headers', '*')
  return res
}

webServer.requestLogging = function (req) {
  let reqString = new Date().toLocaleString().gray
  reqString +=  ' -> ' + req.method + ' to ' + req.url,
  reqString += ' from '.magenta + req.connection.remoteAddress.magenta
  return reqString
}

webServer.fileLoader = function (path) {
  if (!webServer.files[path] && cfg.debug || !cfg.caching) {
    process.stdout.write(' - fresh\n'.green)
  } else {
    process.stdout.write(' - cached\n'.blue)
  }
  return new Promise(resolve => {
    if ((!webServer.files[path]) || (!cfg.caching)) {
      fs.readFile(path, 'utf8', (err, data) => {
        webServer.files[path] = data
        resolve(webServer.files[path])
      })
    } else {
      resolve(webServer.files[path])
    }
  })
}

webServer.sendStatic = function (req, res) {
  let path = `${__dirname}/public`
  if (req.url === '/') {
    path += '/index.html'
  } else {
    path += req.url
  }

  webServer.fileLoader(path)
    .then(file => {
      if (file) {
        return {exists: true, file}
      } else {
        return webServer.fileLoader(`${__dirname}/public/404.html`)
          .then(file => {
            return {exists: false, file}
          })
      }
    }).then(obj => {
      res.writeHead(obj.exists ? 200 : 404)
      res.write(obj.file)
      res.end()
    })
}

webServer.bindDaemon = function (daemon) {
  this.alarms.daemon = daemon
  console.log('Daemon bound to alarms module.')
}
module.exports = webServer
