Create a new alarm by calling `alarm.new.callback(luxonObject, func)`
and it will call the function at the designated time.

You can also call `alarm.new.event(luxonObject, <eventname>)` and it will
emit the event when the alarm goes off.

Finally, you can call `alarm.new.data(luxonObject, anything)` which will simply return a promise resolving with the data. If you pass it a function, the function will not be called.

Calling any of these returns an object specifying when, and contains an ID key so you can call `alarm.delete` on it.

There's also the method `alarm.list` which returns an array of such objects.

If you include this file as a child process,
you can send it messages like:
```Javascript
{
  alarm: '<IS08601 Time String>',
  action: '[add, delete, list]',
  message: '<object or string>'
}
```
and at that time, the whole alarm object will be passed back:
```Javascript
{
  time: '<ISO8601 Time>',
  message: '<something>',
  id: '<something>'
}
```
The alarm is called every day by nature.
As soon as the event is emitted, it is reset for the next day.

When a new alarm is created,
a message is returned with a list of all the alarms and their
IDs.
If your message is improperly formatted, you'll get back:
```Javascript
{
  error: 'rejected',
  message: 'x is wrong'
}
```
message and alarm are not required for action: list.
when calling action: delete, include the key 'id' with the
alarm ID you wish you delete.

To modify an existing alarm, create a new alarm and delete the old one. Don't be lazy.
