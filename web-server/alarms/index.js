const fs = require('fs')
const util = require('util')
const luxon = require('luxon')
const conf = require('../../config/server-config.js')

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const alarmsFile = `${__dirname}/${conf.alarmsFile}`

let alarms = {
  load: readFile(alarmsFile)
    .then(file => {
      console.log('Alarms list loaded.')
      return JSON.parse(file)
    })
    .catch(() => {
      console.error('Alarms file couldn\'t be loaded:', alarmsFile)
      return []
    }),
  alarmsList: [],
  tourbillon: setInterval( this.checkAlarms, 500 ),
}

alarms.create = function (obj) {
  if (!obj.time) {
    return {
      error: true,
      message: 'new alarm has no time',
      obj
    }
  }
  let alarmdt = luxon.DateTime.fromISO(obj.time)
  if (!alarmdt.isValid) {
    return {
      error: true,
      message: 'new alarm\'s time is invalid (ISO only accepted)',
      obj
    }
  }
  alarmdt = alarmdt.set({millisecond: 0})
  if (typeof(obj.callback) != 'function') {
    return {
      error: true,
      message: 'obj.callback is not a function',
      obj
    }
  }
  this.alarmsList.push({
    time: alarmdt.toISOTime(),
    callback: daemon.send('message')
  })
  return this.alarmsList
}

alarms.delete = function () {
  // comment
}

alarms.list = function () {
  return this.alarmsList
}

alarms.checkAlarms = function () {
  this.alarmsList.forEach(alarm => {
    let dt = luxon.DateTime
    if (dt.local().hasSame(dt.fromISO(alarm.time), 'minute')) {
      alarm.callback()
    }
  })
}



module.exports = alarms
