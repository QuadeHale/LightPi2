const luxon = require('luxon')

let alarms = []

if (process.send) {
  process.on('disconnect', () => {
    console.log('Alarm subsys exiting...')
    process.exit(0)
  })
  process.on('message', (m) => {
    if (!m.action) {
      process.send({error: 'rejected', message: 'no action in alarm call'})
      return
    } else if (!['add','delete','list'].includes(m.action)) {
      process.send({error: 'rejected', message: 'alarm action not add, delete or list'})
      return
    }
    if (m.action === 'list') {
      process.send(alarms)
    } else if (m.action === 'delete') {
      if (!m.id) {
        process.send({error: 'rejected', message: 'alarms can\'t delete without an id'})
        return
      }
      alarms = alarms.filter(alarm => {
        if (alarm.id != m.id) {
          return true
        }
      })
      for (let i = 0; i < alarms.length; i++) {
        alarms.id = i
      }
      process.send(alarms)
      return
    } else if (m.action === 'add') {
      if (!m.message) {
        process.send({error: 'rejected', message: 'new alarm has no message'})
      }
      if (!m.time) {
        process.send({error: 'rejected', message: 'new alarm has no time'})
      }
      let alarmdt = luxon.DateTime.fromISO(m.time)
      if (!alarmdt.isValid) {
        process.send({error: 'rejected', message: 'new alarm\'s time is invalid (ISO only accepted)'})
      }
      console.log('New Alarm:', alarmdt.toLocaleString(luxon.DateTime.TIME_24_SIMPLE))
      console.log('Message:', m.message)
      let dt = dt.set({ millisecond: 0 })
      alarms.push({
        time: dt.toISOTime(),
        message: m.message
      })
      alarms[alarms.length-1].id = alarms.length-1
      process.send(alarms)
    }
  })
}

let tourbillon = setInterval( checkAlarms, 500 )
let checkAlarms = function () {
  alarms.forEach(alarm => {
    let dt = luxon.DateTime
    if (dt.local().hasSame(dt.fromISO(alarm.time), 'minute')) {
      process.send(alarm)
    }
  })
}

console.log('Alarm subsystem activated.')
