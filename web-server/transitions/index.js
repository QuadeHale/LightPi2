const fs = require('fs')
const util = require('util')
const conf = require('../../config/server-config.js')

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const transitionFile = `${__dirname}/${conf.transitionFile}`

let transitions = {
  list: readFile(transitionFile)
    .then(file => {
      console.log('Transition list loaded.')
      return JSON.parse(file)
    })
    .catch(() => {
      console.error('Transition file couldn\'t be loaded:', transitionFile)
      return []
    })
}

transitions.create = function (obj) {
  obj.name = obj.name.trim()
  if (this.unique(obj.name)) {
    this.list.push(obj)
    writeFile(transitionFile, this.list)
    return {
      error: false,
      message: 'created',
      obj
    }
  } else {
    return {
      error: true,
      message: 'name not unique',
      obj
    }
  }
}

transitions.delete = function (name) {
  name = name.trim()
  let deleted = false
  this.list = this.list.filter(tObj => {
    if (name === tObj.name) {
      return true
    } else {
      deleted = true
      return false
    }
  })
  writeFile(transitionFile, this.list)
  return {
    error: deleted,
    message: deleted ? name + ' deleted' : 'no match for ' + name
  }
}

transitions.list = function () {
  return this.list
}

transitions.unique = function (name) {
  let unique = true
  this.list.forEach(tObj => {
    if (name.trim() === tObj.name) {
      unique = false
    }
  })
  return unique
}

module.exports = transitions
