'use strict'

const http = require('http')
const cp = require('child_process')
const localIPs = require('./modules/local-ips')
const cfg = require('./config/server-config')
const webServer = require('./web-server')
const colors = require('colors')
let daemon
webServer.daemon = daemon

let validEndpoints = ['/alarms', '/transitions']

let server = http.createServer((req, res) => {
  res = webServer.headers(req, res)
  let payload = ''
  req.on('data', data => payload += data)
  req.on('end', () => {
    if (cfg.debug) process.stdout.write(webServer.requestLogging(req))
    if (req.method === 'POST') {
      if (req.url === '/lights') {
        daemon.once('message', m => {
          res.setHeader('Content-Type', 'application/json; charset=utf-8;')
          res.writeHeader(202)
          res.write(JSON.stringify(m))
          res.end()
        })
        daemon.send(JSON.parse(payload))
      } else if (validEndpoints.includes(req.url)) {
        let response = webServer[req.url.slice(1)].create(payload)
        response.error ? res.writeHeader(400) : res.writeHeader(202)
        res.write(JSON.stringify(response.obj)).end()
      } else {
        res.writeHeader(400)
        res.write('Bad request. POSTs can only be sent to \'/lights\', \'/transitions\' or \'/alarms\'.').end()
      }
    } else if (req.method === 'DELETE') {
      if (validEndpoints.includes(req.url)) {
        let response = webServer[req.url.slice(1)].delete(payload)
        response.error ? res.writeHeader(400) : res.writeHeader(202)
        res.write(JSON.stringify(response.obj)).end()
      } else {
        res.writeHeader(400)
        res.write('Bad request. DELETE can only be sent to \'/alarms\' or \'transitions\'.').end()
      }
    } else if (req.method === 'GET') {
      if (req.url === '/lights') {
        // Get status
        daemon.send({status: true})
        daemon.once('message', m => {
          res.writeHeader(200)
          res.write(JSON.stringify(m))
          res.end()
        })
      } else if (validEndpoints.includes(req.url)) {
        let response = webServer[req.url.slice(1)].list()
        res.write(JSON.stringify(response.obj)).end()
      } else {
        webServer.sendStatic(req, res)
      }
    }
  })
})

if (!module.parent) {
  if (process.argv[2] != 'webonly') {
    console.log(process.argv)
    daemon = cp.fork(`${__dirname}/daemon`)
  } else {
    daemon = cp.fork(`${__dirname}/daemon/fake.js`)
  }
  server.listen(cfg.port, cfg.host, () => {
    let startString = 'LightPi2'.rainbow
    startString += ' listening on ' + cfg.host + ':' + cfg.port
    startString += '\nDevice IP: '+ localIPs.str + ' - File server'
    startString += cfg.caching ? ' IS '.green : ' IS NOT '.red
    startString += 'caching pages.'
    if (process.argv[2] === 'webonly') {
      startString += '- WEB ONLY MODE'.red
    }
    console.log(startString)
  })
}

process.on('SIGINT', () => {
  console.log('Web Server Exiting...'.red)
  server.close()
  if (daemon.connected) {
    console.log('Disconnecting Daemon...'.red)
    daemon.disconnect()
  }
  process.exit()
})
