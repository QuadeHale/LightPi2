'use strict'

// Returns an array of IPv4 Addresses local to the machine.

const os = require('os')
const ifaces = os.networkInterfaces()

let addresses = []
// We have an object containining multiple interfaces:
Object.keys(ifaces).forEach(function (iface) {
  ifaces[iface].forEach(function (el) {
    if (el.family === 'IPv4') {
      if (!el.internal) {
        addresses.push(el.address)
      }
    }
  })
})

let str = addresses.join(', ')
let obj = {ips: addresses, str: str}
if (!module.parent) {
  console.log(obj)
}

module.exports = obj
