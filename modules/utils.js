'use strict'
let hexToRgb = function (hex) {
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return result ? {
    red: (parseInt(result[1], 16)*3.92).toFixed(0),
    green: (parseInt(result[2], 16)*3.92).toFixed(0),
    blue: (parseInt(result[3], 16)*3.92).toFixed(0),
    warm: 0,
    cool: 0
  } : null
}

module.exports = hexToRgb
